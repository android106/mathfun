package buu.withiphong.mathgame.game

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MinusViewModel(correctLast: Int, incorrectLast: Int) : ViewModel() {
    val _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _eventMenu = MutableLiveData<Boolean>()
    val eventMenu: LiveData<Boolean>
        get() = _eventMenu

    private val _eventEndGame = MutableLiveData<Boolean>()
    val eventEndGame: LiveData<Boolean>
        get() = _eventEndGame

    init {
        Log.i("GameViewModel", "GameViewModel created!!!")
        _answer.value = 0
        _correct.value = correctLast
        _incorrect.value = incorrectLast
    }

    fun onCorrect() {
        _correct.value = _correct.value?.plus(1)
    }

    fun onIncorrect() {
        _incorrect.value = _incorrect.value?.plus(1)
    }

    fun onEndGame() {
        _eventEndGame.value = true
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }
}