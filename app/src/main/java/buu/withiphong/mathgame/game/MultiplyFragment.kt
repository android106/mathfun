package buu.withiphong.mathgame.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import buu.withiphong.mathgame.game.MultiplyFragmentArgs
import buu.withiphong.mathgame.game.MultiplyFragmentDirections
import buu.withiphong.mathgame.R
import buu.withiphong.mathgame.databinding.FragmentMultiplyBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MultiplyFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiplyFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentMultiplyBinding
    private lateinit var viewModel: MultiplyViewModel
    private lateinit var viewModelFactory: MultiplyViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multiply, container, false)
        viewModelFactory = MultiplyViewModelFactory(
            MultiplyFragmentArgs.fromBundle(requireArguments()).correct,
            MultiplyFragmentArgs.fromBundle(requireArguments()).incorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MultiplyViewModel::class.java)
        viewModel.correct.observe(viewLifecycleOwner, Observer { newCorrect ->
            binding.textCorrect.text = getString(R.string.scoreCorrect, newCorrect)
        })
        viewModel.incorrect.observe(viewLifecycleOwner, Observer { newIncorrect ->
            binding.textIncorrect.text = getString(R.string.scoreIncorrect, newIncorrect)
        })
        viewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = MultiplyFragmentDirections.actionMultiplyFragmentToScoreFragment(
                    viewModel.correct.value?:0,
                    viewModel.incorrect.value?:0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        binding.multiplyViewModel = viewModel
        binding.lifecycleOwner = this
        generateQuestion()
        checkAnswer(viewModel.correct.value?:0, viewModel.incorrect.value?:0)
        return binding.root
    }

    private fun generateQuestion() {
        binding.apply {
            val randomNum1 = (0..10).random()
            val randomNum2 = (0..10).random()
            num1.text = randomNum1.toString()
            num2.text = randomNum2.toString()
            val ans = randomNum1 * randomNum2
            viewModel._answer.value = ans
            generateAnswer()
        }
    }
    private fun generateAnswer() {
        binding.apply {
            val answers = arrayOf(
                "btnAns1", "btnAns2", "btnAns3"
            )
            val btnRandom = answers[(0..2).random()]
            if (btnRandom == "btnAns1") {
                btnAns1.text = viewModel.answer.value.toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = (0..20).random().toString()
            } else if (btnRandom == "btnAns2") {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = viewModel.answer.value.toString()
                btnAns3.text = (0..20).random().toString()
            } else {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = viewModel.answer.value.toString()
            }
        }
    }
    private fun checkAnswer(correct: Int, incorrect: Int) {
        var correctScore = correct
        var incorrectScore = incorrect
        binding.apply {
            // CheckAnswer
            btnAns1.setOnClickListener {
                val checkAns = btnAns1.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
            btnAns2.setOnClickListener {
                val checkAns = btnAns2.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
            btnAns3.setOnClickListener {
                val checkAns = btnAns3.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
        }
    }
}