package buu.withiphong.mathgame.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.withiphong.mathgame.game.PlusFragmentDirections
import buu.withiphong.mathgame.R
import buu.withiphong.mathgame.databinding.FragmentPlusBinding
import buu.withiphong.mathgame.game.PlusFragmentArgs

class PlusFragment : Fragment() {

    private lateinit var binding: FragmentPlusBinding
    private lateinit var viewModel: PlusViewModel
    private lateinit var viewModelFactory: PlusViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plus, container, false)
        viewModelFactory = PlusViewModelFactory(
            PlusFragmentArgs.fromBundle(requireArguments()).correct,
            PlusFragmentArgs.fromBundle(requireArguments()).incorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(PlusViewModel::class.java)
        viewModel.correct.observe(viewLifecycleOwner, Observer { newCorrect ->
            binding.textCorrect.text = getString(R.string.scoreCorrect, newCorrect)
        })
        viewModel.incorrect.observe(viewLifecycleOwner, Observer { newIncorrect ->
            binding.textIncorrect.text = getString(R.string.scoreIncorrect, newIncorrect)
        })
        viewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = PlusFragmentDirections.actionPlusFragmentToScoreFragment(
                    viewModel.correct.value?:0,
                    viewModel.incorrect.value?:0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        binding.plusViewModel = viewModel
        binding.lifecycleOwner = this
        generateQuestion()
        checkAnswer(viewModel.correct.value?:0, viewModel.incorrect.value?:0)
        return binding.root
    }

    private fun generateQuestion() {
        binding.apply {
            val randomNum1 = (0..10).random()
            val randomNum2 = (0..10).random()
            num1.text = randomNum1.toString()
            num2.text = randomNum2.toString()
            val ans = randomNum1 + randomNum2
            viewModel._answer.value = ans
            generateAnswer()
        }
    }
    private fun generateAnswer() {
        binding.apply {
            val answers = arrayOf(
                "btnAns1", "btnAns2", "btnAns3"
            )
            val btnRandom = answers[(0..2).random()]
            if (btnRandom == "btnAns1") {
                btnAns1.text = viewModel.answer.value.toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = (0..20).random().toString()
            } else if (btnRandom == "btnAns2") {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = viewModel.answer.value.toString()
                btnAns3.text = (0..20).random().toString()
            } else {
                btnAns1.text = (0..20).random().toString()
                btnAns2.text = (0..20).random().toString()
                btnAns3.text = viewModel.answer.value.toString()
            }
        }
    }
    private fun checkAnswer(correct: Int, incorrect: Int) {
        var correctScore = correct
        var incorrectScore = incorrect
        binding.apply {
            // CheckAnswer
            btnAns1.setOnClickListener {
                val checkAns = btnAns1.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
            btnAns2.setOnClickListener {
                val checkAns = btnAns2.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
            btnAns3.setOnClickListener {
                val checkAns = btnAns3.text.toString()
                if (checkAns.toInt() == viewModel.answer.value) {
                    Toast.makeText(activity, "Correct", Toast.LENGTH_SHORT).show()
                    correctScore++
                    viewModel.onCorrect()
                } else {
                    Toast.makeText(activity, "Incorrect", Toast.LENGTH_SHORT).show()
                    incorrectScore++
                    viewModel.onIncorrect()
                }
                generateQuestion()
            }
        }
    }
}