package buu.withiphong.mathgame.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MinusViewModelFactory (private val correctLast: Int, private val incorrectLast: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MinusViewModel::class.java)) {
            return MinusViewModel(correctLast, incorrectLast) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}