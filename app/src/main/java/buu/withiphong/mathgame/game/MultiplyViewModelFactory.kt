package buu.withiphong.mathgame.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MultiplyViewModelFactory (private val correctLast: Int, private val incorrectLast: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultiplyViewModel::class.java)) {
            return MultiplyViewModel(correctLast, incorrectLast) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}