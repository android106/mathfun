package buu.withiphong.mathgame.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PlusViewModelFactory (private val correctLast: Int, private val incorrectLast: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlusViewModel::class.java)) {
            return PlusViewModel(correctLast, incorrectLast) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}