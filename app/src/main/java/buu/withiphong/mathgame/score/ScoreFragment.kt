package buu.withiphong.mathgame.score

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.withiphong.mathgame.R
import buu.withiphong.mathgame.databinding.FragmentScoreBinding
import buu.withiphong.mathgame.game.PlusFragmentArgs

class ScoreFragment : Fragment() {
    private lateinit var binding: FragmentScoreBinding
    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory
    var ScoreCorrect = 0
    var ScoreIncorrect = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentScoreBinding>(inflater, R.layout.fragment_score, container, false)
        viewModelFactory = ScoreViewModelFactory(
            PlusFragmentArgs.fromBundle(requireArguments()).correct,
            PlusFragmentArgs.fromBundle(requireArguments()).incorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)


        binding.PlayAgainButton.setOnClickListener { view ->
            view.findNavController().navigate(
                    ScoreFragmentDirections.actionScoreFragmentToTitleFragment(viewModel.correct.value?:0,viewModel.incorrect.value?:0)
            )
        }

        viewModel.correct.observe(this, Observer { newCorrect ->
            binding.txtCorrect.text = getString(R.string.scoreCorrect, newCorrect)
        })
        viewModel.incorrect.observe(this, Observer { newIncorrect ->
            binding.txtIncorrect.text = getString(R.string.scoreIncorrect, newIncorrect)
        })

        binding.lifecycleOwner = this

        return binding.root
    }
}